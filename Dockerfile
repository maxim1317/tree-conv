RUN apt-get update \
    && apt-get install -y git npm

ENV PORT=9022
ENV INPUT=data.json

WORKDIR /home/webtree

RUN git clone https://github.com/paulirish/webtreemap-cdt/
WORKDIR /home/webtree/webtreemap-cdt
RUN npm i -g webtreemap

WORKDIR /home/webtree

RUN git clone https://gitlab.com/maxim1317/tree-conv/

WORKDIR /home/webtree/tree-conv/

EXPOSE ${PORT}
