#! /bin/bash

echo $INPUT

python3 treeparse.py -i $INPUT -o result.txt \
&& webtreemap -o treemap.html < result.txt \
&& python3 -m http.server $PORT