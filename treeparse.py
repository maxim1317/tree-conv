import sys
from pathlib import Path
import json
import argparse

def _parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog="converter",
        description="Convert json tree to txt.",
    )
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        required=True,
        help="input folder with xml annotation",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=True,
        help="output folder for json annotation",
    )

    try:
        args = parser.parse_args()
        return args
    except (AttributeError, TypeError):
        parser.print_help()
        sys.exit(1)


def parse_json(input_path: str) -> dict:
    """Parse json."""
    with open(input_path, 'r') as jsonfile:
        json_text = jsonfile.read()
    parsed = json.loads(json_text)
    return parsed


def _recursive_parse(tree: dict, result: list) -> list:
    root = tree["name"].split("'")[1] 
    area = str(tree["data"]["$area"] )

    root_record = root + " " + area
    result.append(root_record)

    children = tree.get('children')
    if children:
        for child in children:
            subresult = []
            subresult = _recursive_parse(child, subresult)
            for record in subresult:
                result.append(root + "/" + record)

    return result


def convert(jsontree: dict) -> str:
    """Convert json to webtreemap txt."""
    result_empty = []
    preresult = _recursive_parse(
        tree=jsontree, 
        result=result_empty
    )
    result = []
    for record in preresult:
        split_record = record.split()
        result_record = split_record[1] + " " + split_record[0]
        result.append(result_record)
    result_str = "\n".join(result) + "\n"
    return result_str


def save(converted: str, output_path: str):
    with open(output_path, 'w+') as out:
        out.write(converted)    


def _main():
    args = _parse_args()

    parsed = parse_json(args.input)
    converted = convert(parsed)
    save(converted, args.output)


if __name__ == "__main__":
    _main()
